<?php

return [

    /**
     *
     */
    'api_key' => env('API_KEY', '8eae3a371d57ae59e9a662b0bc25e7f9'),

    /**
     *
     */
    'base_url' => env('API_BASE_URL', 'https://api.themoviedb.org'),

];
